# SILICA
version: 0.0.3

A lightweight Salt management console.

Silica runs on Python 3.6+ and uses Quart web framework (an asyncio version of Flask) to display information about your infrastructure.

Silica works by running a regularly scheduled 'generator' function which connects to Salt-Master via Salt-API and gets various data about your minions. It then stores minion data in a TinyDB JSON database, which is then used for quick lookups.

Silica runs via Hypercorn ASGI framework that manages worker threads and HTTPS.


## Features
- Remote execution from GUI
- Highstate summary
- User authentication system

---
## Requirements
### Python
- python version: 3.6+
- pip packages: *pip, pipenv*
### Saltstack
- Salt version 2018.3+ (tested with 2018.3.4, but will likely work with older versions)

### Operating System
- Tested for: Centos 7, Ubuntu 18.04
- testing for other OSes is welcome, please submit a pull request.

---
## Documentation
- For installation of Silica, see [Installation](docs/1_installation.md)
- For Salt-API and Silica Authentication configuration, see [Configuration](docs/2_configuration.md)
- For Silica usage, see [Usage](docs/3_usage.md)
- Release Notes are found in [Release Notes](docs/release_notes.md)
---
## Roadmap
Features to be added in future versions:
- running Salt-API from an external host (with HTTPS)
- dashboards including node grains, highstate status, etc
- assigning Formulas to minions from the console
- historical data about what state or command was run on each minion
---
## Screenshots
![](app/static/img/s01.png)
![](app/static/img/s02.png)
![](app/static/img/s03.png)
![](app/static/img/s04.png)
![](app/static/img/s05.png)
![](app/static/img/s06.png)
![](app/static/img/s07.png)
![](app/static/img/s08.png)
---
## Additional
Bootstrap version: 4.3.1

Theme:  https://startbootstrap.com/themes/sb-admin-2/