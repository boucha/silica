import os
import sys
from loguru import logger
from quart import Quart, websocket, session
import quart.flask_patch
from quart import Quart
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_apscheduler import APScheduler
from config import BASEDIR, PORT, INTERVAL

LOGDIR = BASEDIR + '/logs' 

if not os.path.exists(LOGDIR):
    os.makedirs(LOGDIR)

logger.add(LOGDIR + '/silica.log', 
    rotation='25MB', 
    colorize=True, 
    format="<green>{time:YYYYMMDD_HH:mm:ss}</green> | {level} | <level>{message}</level>",
    level="DEBUG")

app = Quart(__name__)
app.config.from_object('config')
Bootstrap(app)
 
# Authentication
login = LoginManager(app)
login.session_protection = 'strong'
login.login_view = 'login'
login.refresh_view = 'relogin'
login.needs_refresh_message = (u"Session timedout, please re-login")
login.needs_refresh_message_category = "info"

login.init_app(app)

db = SQLAlchemy(app)


class Config(object):
    SCHEDULER_API_ENABLED = True

scheduler = APScheduler()

from app.views.db.generator import run_generator

if INTERVAL > 0:
    @scheduler.task('interval', id='do_run_generator', minutes=INTERVAL)
    def generator():
        run_generator()
    scheduler.init_app(app)
    scheduler.start()
    logger.info(f'starting generator schedule every {INTERVAL} minutes')

# get absolute app_dir
app_dir = os.path.dirname(os.path.abspath(__file__))
logger.info(f'running silica on port {PORT}')
from app.views import main, errors, minions, run_cmd, login
from app import models