# VIEW MAIN

import json
from dictor import dictor
from app import app
from quart import render_template, session
from loguru import logger
from datetime import timedelta
from flask_login import login_required, logout_user, current_user

@app.before_request
def before_request():
    session.permanent = True
    app.permanent_session_lifetime = timedelta(minutes=app.config['SESSION_LIFETIME'])

@app.route('/')
@app.route('/index')
@login_required
async def index():
    return await render_template("index.html", title='Silica')

@app.route('/dashboard')
@login_required
async def dashboard():
    ''' 
    show statistics dashboard 
    '''
    return await render_template("dashboard.html", title='Silica - Dashboard') 

@app.route('/cheatsheet')
@login_required
async def cheatsheet():
    ''' 
    docs > cheatsheet 
    '''
    return await render_template("cheatsheet.html", title='Silica - Salt Cheatsheet', nav='docs') 

    