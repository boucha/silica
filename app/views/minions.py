from dictor import dictor
from quart import render_template
from loguru import logger
from app import app
from app.views.db import get_minions, get_highstate_totals, get_sync_date
from app.views.db.generator import run_generator
from flask_login import login_required

@app.route('/minion_status')
@login_required
async def minion_status():
    ''' salt minion connection status '''
    
    minions = get_minions()

    if not minions:
        return await render_template('error.html', title='Exception', msg=str('No minion data available, please run Sync DB to generate a new database')), 500
    
    return await render_template("minions.html", title='Silica', sync=get_sync_date(), minions=minions, nav='minions') 

@app.route('/highstate')
@login_required
async def highstate():
    ''' show highstate details '''
    
    totals = get_highstate_totals()

    if not totals:
        return await render_template('error.html', title='Exception', msg=str('No minion data available, please run Sync DB to generate a new database')), 500
    
    return await render_template("highstate.html", title='Silica - Highstate', totals=totals, sync=get_sync_date(), nav='highstate')

@app.route('/generator')
@login_required
async def generator():
    ''' run Generator manually '''
    #app.jinja_env.globals.update(run_generator=run_generator)
 #   result = run_generator()
    #return await render_template("generator.html", generate=lambda x: run_generator() x)
    return await render_template("generator.html", title='Silica - Update DB', nav='generator')

@app.route('/start_generator')
@login_required
async def start_generator():
    ''' run Generator manually '''
    
    result = run_generator()
    
    if result == 'generator done':
        return f'<div class="alert alert-success" role="alert">{result}</div>'    
    else:
        return f'<div class="alert alert-danger" role="alert">{result}</div>'