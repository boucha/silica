import os
import sys
import json
from dictor import dictor
from loguru import logger
from tinydb import TinyDB, Query
import pendulum
from config import BASEDIR
from app.views.errors import unhandled_exception

db_minions = BASEDIR + '/app/views/db/minions.json'
db_highstate = 'highstate.json'

def check_db():
    '''
    checks if a Minion DB has been created
    '''
    if not os.path.exists(db_minions):
        raise Exception('Minion DB file not found, please run Generator to create a new DB')

def get_minions():
    ''' get all minion information '''
    
    check_db()
    
    db = TinyDB(db_minions)

    minions = []

    for value in db.all():
        minions.append(dict(value))

    return minions   

def get_targets():
    '''
    returns list of minions that are active/connected
    '''

    db = TinyDB(db_minions)
    Q = Query()
    
    targets = []
    for target in db.search(Q.status == 'True'):
        targets.append(target['name'])
    return targets

def get_highstate():
    '''
    returns minion highstates
    '''

    db = TinyDB(db_minions)
    Q = Query()
    targets = []
    for target in db.search(Q.status == 'True'):
        targets.append(target['name'])
    return targets

def get_highstate_totals():
    '''
    gets all minions and their highstate results
    returns totals for Changes, No Changes and Failures
    '''
    totals = {}
    minions = get_minions()

    if not minions:
        return None

    for _min in minions:
        name = dictor(_min, 'name')

        if name:
            totals[name] = {}
            summary = dictor(_min, 'highstate')
            
            if summary and isinstance(summary, dict):
                nochanges, changes, failures = 0,0,0

                for hsitem in summary:
                    if summary[hsitem]['result'] is True:
                        nochanges += 1
                    if summary[hsitem]['result'] is None:
                        changes += 1
                    if summary[hsitem]['result'] is False:
                        failures += 1

                totals[name]['nochanges'] = nochanges
                totals[name]['changes'] = changes
                totals[name]['failures'] = failures
                totals[name]['summary'] = summary
            else:
                totals[name]['summary'] = 'Unavailable'
    return totals

def get_sync_date():
    '''
    returns when DB generator was last run (N minutes ago)
    '''
    
    now = pendulum.now()
    db = TinyDB(db_minions)
    Q = Query()
    sync = db.search(Q.sync)[0]['sync']
    
    if sync:
        sync = pendulum.parse(sync)
        difference = now.diff(sync).in_minutes()
        return difference
    else:
        return "No DB data available, run DB Generator to create a new DB file"

