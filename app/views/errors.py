
from flask import request
from quart import render_template
from app import app
from loguru import logger
from flask_login import login_required


# ERROR HANDLERS
@app.errorhandler(404)
@login_required
def page_not_found(error):
    logger.error('Page not found: {}', request.path)
    return render_template('404.html', title='404 Error', msg=request.path)

@app.errorhandler(500)
@login_required
def internal_server_error(error):
    logger.error('Server Error: {}', error)
    return render_template('500.html', title='500 Error', msg=error)

@app.errorhandler(Exception)
@login_required
def unhandled_exception(e):
    logger.error('Unhandled Exception: {}', str(e))
    return render_template('error.html', title='Exception', msg=str(e)), 500

@app.errorhandler(TypeError)
@login_required
def unhandled_type_error(e):
    logger.error('Unhandled TypeError: {}', str(e))
    return render_template('error.html', title='TypeError', msg=str(e)), 500

@app.route('/raise_exception')
@login_required
async def raise_exception(e):
    ''' 
    generate an Exception from within Flask
    '''
    return await render_template("error.html", title='Saltstack - Exception', msg=str(e))
