from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String, Boolean
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from werkzeug.security import generate_password_hash, check_password_hash
import os
import json
import sys
from loguru import logger
from pathlib import Path

app_path = Path(__file__).resolve().parents[3]
db_path = Path(__file__).resolve().parents[0]
# append App to get Silica libs
sys.path.append(str(app_path))

from config import BASEDIR

LOGDIR = BASEDIR + '/logs' 

if not os.path.exists(LOGDIR):
    os.makedirs(LOGDIR)

logger.add(LOGDIR + '/silica.log', 
    rotation='25MB', 
    colorize=True, 
    format="<green>{time:YYYYMMDD_HH:mm:ss}</green> | {level} | <level>{message}</level>",
    level="DEBUG")

auth_dir = BASEDIR + '/app/views/auth'
engine = create_engine('sqlite:///{}/users.db'.format(auth_dir), echo=True)
Base = declarative_base()

class User(Base):
    
    __tablename__ = "users"

    id = Column(Integer, primary_key = True)
    username = Column(String(64), index = True, unique = True)
    password = Column(String(128))
    email = Column(String(120), index = True, unique = True)
    admin = Column(Boolean, default=True, nullable=False)

    def __init__(self, username, email, password, admin):
        self.username = username
        self.email = email
        self.password = password
        self.admin = admin


def add_admins():
    '''
    populate User DB with Silica Admin users from admins.json
    '''
    # create Users DB
    Base.metadata.create_all(engine)

    logger.info('auth: inserting Admin users into users.db')

    # create a Session
    Session = sessionmaker(bind=engine)
    session = Session()

    with open(auth_dir + '/admins.json') as jsondata:
        data = json.load(jsondata)
        for name in data:
            pw_hash = generate_password_hash(data[name]['password'])
            user = User(name, data[name]['email'], pw_hash, 1 )
            session.add(user)
    
    # commit the record the database
    session.commit()

    logger.info('auth: Admin users inserted into users.db')
    logger.info('auth: removing temporary admins.json file')

    #os.remove(auth_dir + '/users.json')



if __name__ == "__main__":
    add_admins()