from sqlalchemy import create_engine, ForeignKey
from sqlalchemy import Column, Date, Integer, String, Boolean
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref
from werkzeug.security import generate_password_hash, check_password_hash
import os
import json
import sys
from loguru import logger
from flask_login import login_required


from config import BASEDIR

auth_dir = BASEDIR + '/app/views/auth'
engine = create_engine('sqlite:///{}/users.db'.format(auth_dir), echo=True)
Base = declarative_base()

class User(Base):
    
    __tablename__ = "users"

    id = Column(Integer, primary_key = True)
    username = Column(String(64), index = True, unique = True)
    password = Column(String(128))
    email = Column(String(120), index = True, unique = True)
    admin = Column(Boolean, default=True, nullable=False)

    def __init__(self, username, email, password, admin):
        self.username = username
        self.email = email
        self.password = password
        self.admin = admin


def add_user(username, password, email):
    '''
    populate User DB with Silica regular users
    '''
    # create Users DB
    Base.metadata.create_all(engine)

    logger.info('auth: inserting regular user into users.db')

    # create a Session
    Session = sessionmaker(bind=engine)
    session = Session()

    pw_hash = generate_password_hash(password)
    user = User(username, email, pw_hash, 0 )
    session.add(user)
    
    # commit the record the database
    session.commit()

    logger.info(f'auth: user {username} inserted into users.db')