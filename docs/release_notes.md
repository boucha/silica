# Silica Release Notes

## 0.0.3
- added authentication system using Flask-Login module
- added Schedule-based Generator - allowing a cron-like scheduling of Highstate output
- updated docs with HTTPS configuration instructions

## 0.0.2
- fixed section highlighting in 'sidebar.html', changed Docs section to remain expanded if clicking on Docs > Cheatsheet
- added Sync status icon to turn yellow and red if last Sync date is greater than 45 minutes (turns yellow) and 60 minutes (turns red)
- added systemd service script for Silica as well as installation bash script that installs Silica + dependencies (tested only on Centos 7)
- added favicon to templates/base.html
- added ability to configure default remote execution settings for Test=True and output Formatter (see config.py)
- added Sync DB functionality, can now manually sync DB from the option on left menu

## 0.0.1
- initial project open sourced