# Silica Usage

Silica uses Salt-API to talk to your Salt-Master (current version only allows localhost setup, future versions will have ability to run Silica on a remote host)


## Generator

Silica runs a "generator" process to pull out various data about your minions. It stores the data in 
```
app/views/db/minions.json
```
This is a TinyDB database which is then used by Silica to display your minion information

In order to get the latest minion data, Silica will run an internal cron and execute a Highstate on all your minions with test=true (simulate a highstate run)

It then stores the output inside 'minions.json' database file.

To configure the highstate generator interval, add the INTERVAL value in config.py

    INTERVAL = 60 (will run highstate test=true every 60 min)

To manually run the Generator, start Silica and click "Sync DB" from the leftside menu, which will run generator function and create/update your minion DB.

![](../app/static/img/ss02.jpg)

---
## Silica Config
### Allowed Commands
Various Silica settings can be configured in the "config.py" file

For example, to limit or expand what commands a user can run for Remote Execution, update the Allowed CMDs dict
```
ALLOWED_CMDS = {
    'group': ['add', 'delete', 'info', 'adduser', 'deluser'],
    'test': ['ping', 'version', 'versions', 'versions_report', 'echo'],
    'status': ['all_status', 'cpuinfo', 'cpustats', 'diskusage'],
    'cmd': ['run'],
    'service': ['get_all'],
    'grains': ['items', 'ls', 'get'],
    'user': ['list_users', 'add', 'delete', 'getent', 'info', 'list_groups', 'rename', 'chgid', 'chgroups'],
    'state': ['highstate']
}
```

If you need to change the port on which Silica runs, update the PORT variable in config.py

### Remote Execution
by default, Silica will run all your remote execution commands with "test=true" flag set to ON, meaning your commands will be simulated and not actually applied to the target. This is done for safety. 

To disable this feature, update the Simulate flag to False
```
USER_OPTIONS = {
    'simulate': True,  # set all remote exec commands to run with 'test=true' by default?
    'output': 'json'    # default remote exec command output (json, yaml)
}
```

You can also control the output formatter with which Salt will return your data. The default formatter is JSON. 