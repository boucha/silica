# Troubleshooting Silica

## can't execute remote commands
make sure you can execute anything from your salt master using the 'silica' user

```
root@master> salt -a pam --username=silica --password=silica \* test.ping 

minion1:
    True
```

