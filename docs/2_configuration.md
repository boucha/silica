# Silica Configuration

In order for Silica to be useful, it needs to use Salt-API to pull data from your Salt environment.

## Configure SaltStack with Silica
install SaltStack OS packges (ignore if Salt Master, Minion and API is already installed in your environment)

```
yum -y install salt-master salt-minion salt-api
```

### create cert
```
openssl genrsa -out /etc/ssl/certs/saltapi_key.pem 4096
openssl req -new -x509 -key /etc/ssl/certs/saltapi_key.pem -out /etc/ssl/certs/saltapi_cert.pem -days 1826

hit Enter for every prompt, this will generate the Cert
```

### configure Salt API


```
root@saltmaster> vim /etc/salt/master.d/salt-api.conf

rest_cherrypy:
  port: 9709
  ssl_crt: /etc/ssl/certs/saltapi_cert.pem
  ssl_key: /etc/ssl/certs/saltapi_key.pem
```

### configure authentication for Silica service account

```
root@saltmaster> vim /etc/salt/master.d/auth.conf

external_auth:
  pam:
    silica:
      - .* 
```

restart Salt Master
```
root@saltmaster> systemctl restart salt-master
```

Open up config.py
add your Salt-API creds to Salt-API dictionary,

    SALT_API = {
    'user': 'silica',
    'pw': 'silica',
    'host': 'localhost',
    'port': 9709,
    'allowed_users': 'root'
}


Optional: add read/write to Master log for Silica user
```
root@saltmaster> setfacl -Rm u:silica:rwx /var/log/salt
```


### start Salt-API
``` 
root@saltmaster> systemctl start salt-api.service
```

### Test API usage

get token
```
 curl -ski https://localhost:9709/login -H 'Accept: application/json' -d username='silica' -d password='silica123' -d eauth='pam'

 generates token: bddae5de1962a42ef26dbbf5a406eb528524413f
```


make a call
```
curl -sk https://localhost:9709/minions -H 'Accept: application/json' -H 'X-Auth-Token: bddae5de1962a42ef26dbbf5a406eb528524413f' -d client='local' -d tgt='minion01' -d fun='test.ping'
```

---
## Setup Silica GUI Authentication
To allow user access to Silica web console, you must create users inside Silica's User database. 
This is an important security step since users can potentially run any command on your minions as 'root' from the Silica console.

Define your Silica console users,
```
# cd /home/silica/silica/app/views/auth, 
# open "users.json" for editing, add your users who will need console access

{
    "admin": { "email": "admin@company.com", "password": "spAceb@11s" },
    "bob": { "bob": "bob@company.com", "password": "ludicr0usSp33d"}
}
```
Now generate your users,

    cd /home/silica/silica
    pipenv run create_admins  

Users will now be added to the User DB, passwords are hashed. The temporary users.secure.json file will be deleted once this process is completed.

--
## Optional - Run Silica with HTTPS
To secure Silica with HTTPS, add the paths to your cert and key files to app.py
    
    if __name__ == '__main__':
      app.run(host='0.0.0.0', port=config.PORT, debug=False, use_reloader=True,     certfile='/etc/ssl/certs/saltapi_cert.pem', keyfile='/etc/ssl/certs/saltapi_key.pem')

Also update your Pipfile to have Hypercorn use your certs,

    [scripts]
    silica = 'hypercorn app:app --certfile /etc/ssl/certs/saltapi_cert.pem --keyfile /etc/ssl/certs/saltapi_key.pem  -k asyncio -w 1 -b 0.0.0.0:5100 --reload'

This will force Silica to run via HTTPS and encrypt all data.

---
### Troubleshooting

in case of errors, see [Troubleshooting](troubleshooting.md)
---
